# React Node Typescript Boilerplate

This is a boilerplate project that uses react for the front end and NodeJS as the backend

Both the client and server are written in [TypeScript](https://www.typescriptlang.org), using [React](https://reactjs.org) and [NodeJS](https://nodejs.org)

## Getting Started

- Install NodeJS Version `v17.1.0`
- Install Yarn Version `v1.22.17`
- Install all dependencies for both `client` and `server`

  ```
  cd ./client && yarn && cd ../server && yarn
  ```

- Start Express server

  ```
  cd ./server && yarn start
  ```

- Start React App
  ```
  cd ./client && yarn start
  ```
