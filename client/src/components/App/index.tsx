import React, { FC } from 'react';

const App: FC = () => {
  return (
    <div className="App">
      <p>Hello world</p>
    </div>
  );
};

export default App;
